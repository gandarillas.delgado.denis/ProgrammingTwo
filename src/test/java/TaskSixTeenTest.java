import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKSIXTEEN.Logic.Calculator;
import TASKSIXTEEN.Logic.Operations;
import TASKSIXTEEN.Logic.Verifier;
import TASKSIXTEEN.Messages.OperationException;
import TASKSIXTEEN.Messages.VerifierException;

public class TaskSixTeenTest {
    
    private Calculator calculator = new Calculator();
    private Operations operations = new Operations();
    private Verifier verifier = new Verifier();

    @Test
    public void AdditiosOperation() throws OperationException{
        assertEquals(2, operations.additionOperation(1, 1));
        assertEquals(52, operations.additionOperation(27, 25));
    }

    @Test
    public void multiplicationOperation() throws OperationException{
        assertEquals(516, operations.multiplicationOperation(12, 43));
        assertEquals(116, operations.multiplicationOperation(4, 29));
    }

    @Test
    public void factorialOperation() throws OperationException{
        assertEquals(24, operations.factorialOperation(4)); //109641728
        assertEquals(109641728, operations.factorialOperation(19));
    }

    @Test
    public void powOperation() throws OperationException{
        assertEquals(20736, operations.powOperation(12, 4));
        assertEquals(49, operations.powOperation(7, 2));
    }

    @Test
    public void CalculatorOperation() throws VerifierException, OperationException{
        assertEquals(71, calculator.calculator("4!+(20+2+(1^2*1+4!))"));
        assertEquals(25575, calculator.calculator("19+12*(9^2+6!+(12*2))"));
        assertEquals(6, calculator.calculator("1+2+3"));
        assertEquals(20, calculator.calculator("2*2*5"));
        assertEquals(10, calculator.calculator("2^3+2"));
        assertEquals(15, calculator.calculator("1+2*5"));
        assertEquals(130, calculator.calculator("5!+10"));
        assertEquals(36, calculator.calculator("3!^2"));
        assertEquals(48, calculator.calculator("4^2*3")); //-> 4^2*3 -> 16*3 -> 48
        assertEquals(1468, calculator.calculator("3^3!+5*2"));
        assertEquals(6, calculator.calculator("(1+2)+3"));
        assertEquals(20, calculator.calculator("2*(2*5)"));
        assertEquals(10, calculator.calculator("(2^3)+2"));
        assertEquals(32, calculator.calculator("2^(3+2)")); //-> 2^5 -> 32
        assertEquals(15, calculator.calculator("(1+2)*5"));
        assertEquals(11, calculator.calculator("1+(2*5)"));
        assertEquals(120, calculator.calculator("(3+2)!"));
        assertEquals(36, calculator.calculator("(3!)^2"));
        assertEquals(4096, calculator.calculator("4^(2*3)"));
        assertEquals(739, calculator.calculator("3^3!+(5*2)"));
    }
    
    @Test
    public void AdditiosOperationOutRange() {
        try {
            operations.additionOperation(2147483647, 1);
        } catch (Exception e) {
            assertEquals("The addition result is greater than the allowable range.\n\n\t\t\t -> OPERATION: 2147483647+1", e.getMessage());
        }
    }

    @Test
    public void multiplicationOperationOutRange() {
        try {
            operations.multiplicationOperation(2147483647, 2);
        } catch (OperationException e) {
            assertEquals("The multiplication result is greater than the allowable range.\n\n\t\t\t -> OPERATION: 2147483647*2", e.getMessage());
        }
    }

    @Test
    public void factorialOperationOutRange() {
        try {
            operations.factorialOperation(40);
        } catch (OperationException e) {
            assertEquals("The factorial result is greater than the allowable range.\n\n\t\t\t -> OPERATION: 40!", e.getMessage());
        }
    }

    @Test
    public void powOperationOutRange() {
        try {
            operations.powOperation(12, 40);
        } catch (OperationException e) {
            assertEquals("The power result is greater than the allowable range.\n\n\t\t\t -> OPERATION: 12^40", e.getMessage());
        }
    }

    @Test
    public void bracketsVerification(){
        assertEquals(true, verifier.verifyBrackets("(2+1+(1+1))"));
        assertEquals(false, verifier.verifyBrackets("(2+1+(1+1)))"));
    }

    @Test
    public void noLetters(){
        assertEquals(false, verifier.verifyNotLetters("1+1+12*6"));
        assertEquals(true, verifier.verifyNotLetters("1+1+12*6a"));
    }

}
