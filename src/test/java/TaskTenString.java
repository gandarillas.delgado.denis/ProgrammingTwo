import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import TASKTEN.JULinkedList;
import TASKTEN.Lista;

public class TaskTenString {

    private Lista<String> listString = new JULinkedList<>();

    public void addElementsLL() {
        listString.add("a");
        listString.add("b");
        listString.add("c");
        listString.add("d");
        listString.add("e");
        listString.add("f");
        listString.add("g");
        listString.add("h");
        listString.add("i");
        listString.add("j");
        listString.add("k");
        listString.add("l");
        listString.add("l");
        listString.add("m");
        listString.add("n");
        listString.add("o");
        listString.add("p");
        listString.add("q");
    }

    @Test
    public void LinkedListEmpty() {
        assertEquals(true, listString.isEmpty());
    }

    @Test
    public void addElements() {
        assertEquals(true, listString.add("DENIS"));
    }

    @Test
    public void sizeElement() {
        addElementsLL();
        assertEquals(18, listString.size());
    }

    @Test
    public void cleanList() {
        addElementsLL();
        assertEquals(18, listString.size());
        listString.clear();
        assertEquals(0, listString.size());
    }

    @Test
    public void getElement() {
        addElementsLL();
        assertEquals("f", listString.get(5));
    }

    @Test
    public void removeElements() {
        addElementsLL();
        assertEquals("g", listString.remove(6));
        Object number = 1;
        assertEquals(false, listString.remove(number));
    }

    @Test
    public void noRemoveElements() {
        addElementsLL();
        assertEquals(null, listString.remove(20));
        Object letter = "z";
        assertEquals(false, listString.remove(letter));
    }

    @Test
    public void setElement() {
        addElementsLL();
        assertEquals("g", listString.set(6, "20"));
    }

    @Test
    public void firstElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("IF");
        listString.add("for");

        assertEquals(1, listString.indexOf("Progra"));

    }

    @Test
    public void lastElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("IF");
        listString.add("for");

        assertEquals(11, listString.lastIndexOf("Progra"));

    }

    @Test
    public void noFirstElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("IF");
        listString.add("for");

        assertEquals(0, listString.indexOf("Universidad"));

    }

    @Test
    public void noLastElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("IF");
        listString.add("for");

        assertEquals(0, listString.lastIndexOf("Nachos"));

    }

    @Test
    public void containsElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Programa");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("Jojo");
        listString.add("for");

        assertEquals(true, listString.contains("Jojo"));

    }

    @Test
    public void noContainsElement() {
        listString.add("Pepe");
        listString.add("Progra");
        listString.add("4");
        listString.add("Programacion");
        listString.add("JALA");
        listString.add("Progra");
        listString.add("Programa");
        listString.add("DENIS");
        listString.add("Jojo");
        listString.add("Progra");
        listString.add("Progra");
        listString.add("Bolivia");
        listString.add("Jojo");
        listString.add("for");

        assertEquals(false, listString.contains("letter"));

    }

}
