import static org.junit.jupiter.api.Assertions.assertEquals;

import TASKFOUR.Kata;

public class TaskFourTest {
  @org.junit.jupiter.api.Test
  public void tests() {
    assertEquals(2, Kata.mostFrequentItemCount(new int[] {3, -1, -1}));
    assertEquals(5, Kata.mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
    assertEquals(9, Kata.mostFrequentItemCount(new int [] {2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4}));
  }
}