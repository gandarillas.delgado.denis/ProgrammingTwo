import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKTEN.JULinkedList;
import TASKTEN.Lista;

public class TaskTenInteger {

    private Lista<Integer> listInteger = new JULinkedList<>();

    public void addElementsLL(){
        for (int index = 0; index < 10; index++) {
            listInteger.add(index);
        }
    }
    
    @Test
    public void LinkedListEmpty(){
        assertEquals(true, listInteger.isEmpty());
    }

    @Test
    public void addElements(){
        assertEquals(true, listInteger.add(1));
    }

    @Test
    public void sizeElement(){
        addElementsLL();
        assertEquals(10, listInteger.size());
    }

    @Test
    public void cleanList(){
        addElementsLL();
        assertEquals(10, listInteger.size());
        listInteger.clear();
        assertEquals(0, listInteger.size());
    }

    @Test
    public void getElement(){
        addElementsLL();
        assertEquals(5, listInteger.get(5));
    }

    @Test
    public void removeElements(){
        addElementsLL();
        assertEquals(6, listInteger.remove(6));
        Object number = 1;
        assertEquals(true, listInteger.remove(number));
    }

    @Test
    public void noRemoveElements(){
        addElementsLL();
        assertEquals(null, listInteger.remove(11));
        Object number = 20;
        assertEquals(false, listInteger.remove(number));
    }

    @Test
    public void setElement(){
        addElementsLL();
        assertEquals(6, listInteger.set(6, 20));
    }

    @Test
    public void firstElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        assertEquals(1, listInteger.indexOf(2));

    }

    @Test
    public void lastElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        assertEquals(11, listInteger.lastIndexOf(2));

    }

    @Test
    public void noFirstElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        assertEquals(0, listInteger.indexOf(12));

    }

    @Test
    public void noLastElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        assertEquals(0, listInteger.lastIndexOf(12));

    }

    @Test
    public void containsElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        Object number = 5;
        assertEquals(true, listInteger.contains(number));

    }

    @Test
    public void noContainsElement(){
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(4);
        listInteger.add(23);
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(5);
        listInteger.add(55);
        listInteger.add(2);
        listInteger.add(2);
        listInteger.add(89);
        listInteger.add(0);
        listInteger.add(9);

        Object number = 65;
        assertEquals(false, listInteger.contains(number));

    }

    @Test
    public void testIterator(){
        int counter = 0;
        while (listInteger.iterator().hasNext()){
            assertEquals(listInteger.get(counter), listInteger.iterator().next());
        }
        assertEquals(0, counter);
    }

}