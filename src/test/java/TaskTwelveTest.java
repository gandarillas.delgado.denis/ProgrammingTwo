import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKTWELVE.*;

public class TaskTwelveTest {

    @Test
    public void selectionSort() {
        Bag<Integer> bagSelection = new LinkedBag<>();
        bagSelection.add(5);
        bagSelection.add(4);
        bagSelection.add(1);
        bagSelection.add(2);
        bagSelection.add(3);
        bagSelection.add(7);
        bagSelection.add(9);
        bagSelection.add(6);
        bagSelection.add(8);

        assertEquals("(9)root={data=8, sig->{data=6, sig->{data=9, sig->{data=7, sig->{data=3, sig->{data=2, sig->{data=1, sig->{data=4, sig->{data=5, sig->null}}}}}}}}}", bagSelection.toString());

        bagSelection.selectionSort();

        assertEquals("(9)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=6, sig->{data=7, sig->{data=8, sig->{data=9, sig->null}}}}}}}}}", bagSelection.toString());

    }

    @Test
    public void bubbleSort(){
        Bag<Integer> bagBubble = new LinkedBag<>();
        bagBubble.add(5);
        bagBubble.add(4);
        bagBubble.add(9);
        bagBubble.add(6);
        bagBubble.add(8);
        bagBubble.add(1);
        bagBubble.add(2);
        bagBubble.add(3);
        bagBubble.add(7);

        assertEquals("(9)root={data=7, sig->{data=3, sig->{data=2, sig->{data=1, sig->{data=8, sig->{data=6, sig->{data=9, sig->{data=4, sig->{data=5, sig->null}}}}}}}}}", bagBubble.toString());

        bagBubble.bubbleSort();

        assertEquals("(9)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=6, sig->{data=7, sig->{data=8, sig->{data=9, sig->null}}}}}}}}}", bagBubble.toString());
    }

}
