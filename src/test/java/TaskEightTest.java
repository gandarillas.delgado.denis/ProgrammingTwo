import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKEIGHT.JUArrayList;

public class TaskEightTest {

    private JUArrayList juArrayList = new JUArrayList();

    @Test
    public void emptyArray() {
        assertEquals(false, juArrayList.isEmpty());
    }

    public void addElements(){
        juArrayList.clear();
        for (int i = 1; i < 15; i++) {
            juArrayList.add(i);
        }
    }

    @Test
    public void addElementsArray(){
        assertEquals(true, juArrayList.add(3));
    }

    @Test
    public void sizeArray() {

        addElements();
        assertEquals(14, juArrayList.size());
    }

    @Test
    public void removeElement(){

        addElements();
        assertEquals(4, juArrayList.remove(3));

    }

    @Test
    public void removeElementSpecificTrue(){
        Object elementRemove = 3;
        addElements();

        assertEquals(true, juArrayList.remove(elementRemove));
    }

    @Test
    public void removeElementSpecificFalse(){
        Object elementRemove = 16;
        addElements();

        assertEquals(false, juArrayList.remove(elementRemove));
    }

    @Test
    public void clearArray(){
        addElements();
        assertEquals(14, juArrayList.size());

        juArrayList.clear();
        assertEquals(1, juArrayList.size());

        assertEquals(true, juArrayList.isEmpty());

    }

    @Test
    public void getElement(){
        addElements();

        assertEquals(3, juArrayList.get(2));
    }
}
