import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKFIFTEEN.ListaNode;
import TASKFIFTEEN.Stack;

public class TaskTaskFifteen {

    private ListaNode<Integer> stackInteger = new Stack<>();
    private ListaNode<String> stackString = new Stack<>();

    @Test
    public void stackEmptyInteger() {

        assertEquals(null, stackInteger.pop());

    }

    @Test
    public void stackEmptyString() {

        assertEquals(null, stackString.pop());

    }

    @Test
    public void pushStackInteger() {

        assertEquals(12, stackInteger.push(12));
        assertEquals(68, stackInteger.push(68));
        assertEquals(80, stackInteger.push(80));
        assertEquals(21, stackInteger.push(21));

    }

    @Test
    public void pushStackString() {

        assertEquals("Java", stackString.push("Java"));
        assertEquals("JavaScript", stackString.push("JavaScript"));
        assertEquals("PHP", stackString.push("PHP"));
        assertEquals("Python", stackString.push("Python"));
        assertEquals("C++", stackString.push("C++"));

    }

    @Test
    public void popStackInteger(){

        addElementsInteger();

        assertEquals(21, stackInteger.pop());
        assertEquals(80, stackInteger.pop());
        assertEquals(68, stackInteger.pop());
        assertEquals(12, stackInteger.pop());
        assertEquals(null, stackInteger.pop());

    }

    @Test
    public void popStackString(){

        addElementsString();

        assertEquals("C++", stackString.pop());
        assertEquals("Python", stackString.pop());
        assertEquals("PHP", stackString.pop());
        assertEquals("JavaScript", stackString.pop());
        assertEquals("Java", stackString.pop());
        assertEquals(null, stackString.pop());

    }

    // Methods for adding elements

    public void addElementsInteger() {

        stackInteger.push(12);
        stackInteger.push(68);
        stackInteger.push(80);
        stackInteger.push(21);

    }

    public void addElementsString() {

        stackString.push("Java");
        stackString.push("JavaScript");
        stackString.push("PHP");
        stackString.push("Python");
        stackString.push("C++");

    }
}
