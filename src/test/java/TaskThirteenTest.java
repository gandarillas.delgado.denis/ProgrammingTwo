import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import TASKTHIRTEEN.CircularLinkedList;
import TASKTHIRTEEN.Lista;

public class TaskThirteenTest {

    private Lista<Integer> listaInteger = new CircularLinkedList<>();
    
    @Test
    public void isEmpty(){
        assertEquals(true, listaInteger.isEmpty());
        assertEquals(0, listaInteger.size());
    }

    @Test
    public void addElements(){
        listaInteger.addElementsInteger();
        assertEquals(10, listaInteger.get(0));
        assertEquals(1, listaInteger.get(1));
        assertEquals(5, listaInteger.get(2));
        assertEquals(8, listaInteger.get(3));
        assertEquals(2, listaInteger.get(4));
        assertEquals(13, listaInteger.get(5));
        assertEquals(1, listaInteger.get(6));
        assertEquals(2, listaInteger.get(7));
        assertEquals(11, listaInteger.get(8));
        assertEquals(16, listaInteger.get(9));
    }

    @Test
    public void circularList(){
        listaInteger.addElementsInteger();
        assertEquals(10, listaInteger.get(0));
        assertEquals(1, listaInteger.get(1));
        assertEquals(5, listaInteger.get(2));
        assertEquals(8, listaInteger.get(3));
        assertEquals(2, listaInteger.get(4));
        assertEquals(13, listaInteger.get(5));
        assertEquals(1, listaInteger.get(6));
        assertEquals(2, listaInteger.get(7));
        assertEquals(11, listaInteger.get(8));
        assertEquals(16, listaInteger.get(9));
        assertEquals(10, listaInteger.get(10));
        assertEquals(1, listaInteger.get(11));
        assertEquals(5, listaInteger.get(12));
        assertEquals(8, listaInteger.get(13));
        assertEquals(2, listaInteger.get(14));
        assertEquals(13, listaInteger.get(15));
        assertEquals(1, listaInteger.get(16));
        assertEquals(2, listaInteger.get(17));
        assertEquals(11, listaInteger.get(18));
        assertEquals(16, listaInteger.get(19));
    }

    @Test
    public void notIsEmpty(){
        listaInteger.addElementsInteger();
        assertEquals(false, listaInteger.isEmpty());
        assertEquals(10, listaInteger.size());
    }

    @Test
    public void mergeSort(){
        listaInteger.addElementsInteger();
        listaInteger.mergeSort();
        assertEquals(1, listaInteger.get(0));
        assertEquals(1, listaInteger.get(1));
        assertEquals(2, listaInteger.get(2));
        assertEquals(2, listaInteger.get(3));
        assertEquals(5, listaInteger.get(4));
        assertEquals(8, listaInteger.get(5));
        assertEquals(10, listaInteger.get(6));
        assertEquals(11, listaInteger.get(7));
        assertEquals(13, listaInteger.get(8));
        assertEquals(16, listaInteger.get(9));
    }

    @Test
    public void mergeSortCircular(){
        listaInteger.addElementsInteger();
        listaInteger.mergeSort();
        assertEquals(1, listaInteger.get(0));
        assertEquals(1, listaInteger.get(1));
        assertEquals(2, listaInteger.get(2));
        assertEquals(2, listaInteger.get(3));
        assertEquals(5, listaInteger.get(4));
        assertEquals(8, listaInteger.get(5));
        assertEquals(10, listaInteger.get(6));
        assertEquals(11, listaInteger.get(7));
        assertEquals(13, listaInteger.get(8));
        assertEquals(16, listaInteger.get(9));
        assertEquals(1, listaInteger.get(10));
        assertEquals(1, listaInteger.get(11));
        assertEquals(2, listaInteger.get(12));
        assertEquals(2, listaInteger.get(13));
        assertEquals(5, listaInteger.get(14));
        assertEquals(8, listaInteger.get(15));
        assertEquals(10, listaInteger.get(16));
        assertEquals(11, listaInteger.get(17));
        assertEquals(13, listaInteger.get(18));
        assertEquals(16, listaInteger.get(19));
    }

    @Test
    public void turnRightWithoutOrder(){
        listaInteger.addElementsInteger();
        listaInteger.rotate(4);
        assertEquals(1, listaInteger.get(0));
        assertEquals(2, listaInteger.get(1));
        assertEquals(11, listaInteger.get(2));
        assertEquals(16, listaInteger.get(3));
        assertEquals(10, listaInteger.get(4));
        assertEquals(1, listaInteger.get(5));
        assertEquals(5, listaInteger.get(6));
        assertEquals(8, listaInteger.get(7));
        assertEquals(2, listaInteger.get(8));
        assertEquals(13, listaInteger.get(9));
    }

    @Test
    public void turnLeftWithoutOrder(){
        listaInteger.addElementsInteger();
        listaInteger.rotate(-6);
        assertEquals(1, listaInteger.get(0));
        assertEquals(2, listaInteger.get(1));
        assertEquals(11, listaInteger.get(2));
        assertEquals(16, listaInteger.get(3));
        assertEquals(10, listaInteger.get(4));
        assertEquals(1, listaInteger.get(5));
        assertEquals(5, listaInteger.get(6));
        assertEquals(8, listaInteger.get(7));
        assertEquals(2, listaInteger.get(8));
        assertEquals(13, listaInteger.get(9));
    }

    @Test
    public void turnRightInOrder(){
        listaInteger.addElementsInteger();
        listaInteger.mergeSort();
        listaInteger.rotate(7);
        assertEquals(2, listaInteger.get(0));
        assertEquals(5, listaInteger.get(1));
        assertEquals(8, listaInteger.get(2));
        assertEquals(10, listaInteger.get(3));
        assertEquals(11, listaInteger.get(4));
        assertEquals(13, listaInteger.get(5));
        assertEquals(16, listaInteger.get(6));
        assertEquals(1, listaInteger.get(7));
        assertEquals(1, listaInteger.get(8));
        assertEquals(2, listaInteger.get(9));
    }

    @Test
    public void turnLeftInOrder(){
        listaInteger.addElementsInteger();
        listaInteger.mergeSort();
        listaInteger.rotate(-2);
        assertEquals(2, listaInteger.get(0));
        assertEquals(2, listaInteger.get(1));
        assertEquals(5, listaInteger.get(2));
        assertEquals(8, listaInteger.get(3));
        assertEquals(10, listaInteger.get(4));
        assertEquals(11, listaInteger.get(5));
        assertEquals(13, listaInteger.get(6));
        assertEquals(16, listaInteger.get(7));
        assertEquals(1, listaInteger.get(8));
        assertEquals(1, listaInteger.get(9));
    }

}
