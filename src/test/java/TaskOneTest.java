import static org.junit.jupiter.api.Assertions.assertEquals;

import TASKONE.People;

public class TaskOneTest {

    @org.junit.jupiter.api.Test
    public void testSample() {
        People person = new People(25, "Adam", "Savage", "San Francisco", "Unchained Reaction");
        assertEquals(
                "Adam",
                person.getName());
        assertEquals(
                "hello my name is Adam",
                person.greet());
    }
}
