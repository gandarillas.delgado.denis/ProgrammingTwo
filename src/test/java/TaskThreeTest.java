import static org.junit.jupiter.api.Assertions.assertEquals;

import TASKTHREE.Canvas;

public class TaskThreeTest {

    @org.junit.jupiter.api.Test
    public void drawLines() {

        Canvas canvas = new Canvas(5, 5);
        canvas.draw(0, 2, 4, 2).draw(2, 0, 2, 4);
        assertEquals("-------\n|  x  |\n|  x  |\n|xxxxx|\n|  x  |\n|  x  |\n-------", canvas.drawCanvas());
    }

}
