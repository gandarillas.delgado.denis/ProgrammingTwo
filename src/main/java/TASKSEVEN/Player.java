package TASKSEVEN;

import java.util.Scanner;

public class Player {

    private String skinPlayer;
    private int xPosition;
    private int yPosition;
    private boolean verifyPlayerGenerator;
    private Scanner scanner;
    private Message message;

    public Player(int xPosition, int yPosition) {
        skinPlayer = Character.toString(128_512) + " ";
        verifyPlayerGenerator = false;
        message = new Message();
        scanner = new Scanner(System.in);
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public void generatorPlayer(String[][] mapGame) {

        while (!verifyPlayerGenerator) {
            if (mapGame[yPosition][xPosition] != Obstacles.getObstacles() && (xPosition != 9 && yPosition != 9)) {
                mapGame[yPosition][xPosition] = skinPlayer;
                verifyPlayerGenerator = true;
            }
        }

    }

    public void movePlayer(String[][] mapGame) {
        message.enterMovements();
        String auxiliar;
        String move = scanner.nextLine();
        int auxiliarXPosition = 0;
        int auxiliarYPosition = 0;
        switch (move.toUpperCase()) {
            case "A":

                auxiliarXPosition = xPosition - 1;

                if (auxiliarXPosition >= 0) {
                    if (mapGame[yPosition][auxiliarXPosition] == "*  ") {
                        auxiliar = mapGame[yPosition][auxiliarXPosition];
                        mapGame[yPosition][auxiliarXPosition] = skinPlayer;
                        mapGame[yPosition][xPosition] = auxiliar;
                        xPosition = xPosition - 1;
                    }
                }

                if (auxiliarXPosition == -1 || mapGame[yPosition][auxiliarXPosition] == Obstacles.getObstacles()) {
                    message.messageErrorMove();
                }
                break;
            case "D":

                auxiliarXPosition = xPosition + 1;

                if (auxiliarXPosition < 10) {
                    if (mapGame[yPosition][auxiliarXPosition] == "*  "
                            || mapGame[yPosition][auxiliarXPosition] == GameMap.getFinish()) {
                        auxiliar = mapGame[yPosition][auxiliarXPosition];
                        mapGame[yPosition][auxiliarXPosition] = skinPlayer;
                        mapGame[yPosition][xPosition] = auxiliar;
                        xPosition = auxiliarXPosition;
                    }
                }
                if (auxiliarXPosition == 10 || mapGame[yPosition][auxiliarXPosition] == Obstacles.getObstacles()) {
                    message.messageErrorMove();
                }

                break;
            case "W":

                auxiliarYPosition = yPosition - 1;

                if (auxiliarYPosition >= 0) {
                    if (mapGame[auxiliarYPosition][xPosition] == "*  ") {
                        auxiliar = mapGame[auxiliarYPosition][xPosition];
                        mapGame[auxiliarYPosition][xPosition] = skinPlayer;
                        mapGame[yPosition][xPosition] = auxiliar;
                        yPosition = auxiliarYPosition;
                    }
                }
                if (auxiliarYPosition == -1 || mapGame[auxiliarYPosition][xPosition] == Obstacles.getObstacles()) {
                    message.messageErrorMove();
                }

                break;
            case "S":

                auxiliarYPosition = yPosition + 1;
                if (auxiliarYPosition < 10) {
                    if (mapGame[auxiliarYPosition][xPosition] == "*  "
                            || mapGame[auxiliarYPosition][xPosition] == GameMap.getFinish()) {
                        auxiliar = mapGame[auxiliarYPosition][xPosition];
                        mapGame[auxiliarYPosition][xPosition] = skinPlayer;
                        mapGame[yPosition][xPosition] = auxiliar;
                        yPosition = auxiliarYPosition;
                    }
                }
                if (auxiliarYPosition == 10 || mapGame[auxiliarYPosition][xPosition] == Obstacles.getObstacles()) {
                    message.messageErrorMove();
                }

                break;
            default:
                message.messageErrorMove();
                break;
        }
    }

    public String getSkinplayer() {
        return skinPlayer;
    }

    public int getXPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

}
