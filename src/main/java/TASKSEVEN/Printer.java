package TASKSEVEN;

public class Printer {
    
    public void printMessage(String message){
        System.out.println(message);
    }

    public void printMessageError(String message){
        System.out.println(Colors.RED+message+Colors.RESET);
    }

    public void printMessageTitle(String message){
        System.out.println(Colors.PURPLE_BACKGROUND+Colors.WHITE+message+Colors.RESET);
    }

    public void printerMapSections(String section){
        System.out.print(section);
    }

    public void lineBreak(){
        System.out.println();
    }

    public void printAvailableKeys(char key, String functionKey){
        System.out.println("["+key+"] "+functionKey);
    }

    public void printCoords(int x, int y){
        System.out.println("X: "+x+"\t|\tY: "+y);
    }

    public void printFinish(String finish){
        System.out.println(Colors.PURPLE_BACKGROUND+Colors.WHITE+finish+Character.toString(127_942)+Colors.RESET);
    }

}
