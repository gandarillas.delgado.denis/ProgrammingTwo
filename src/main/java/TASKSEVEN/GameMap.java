package TASKSEVEN;

public class GameMap {

    private String[][] mapGame = new String[10][10];
    private static final String WAY = "*  ";
    private static final String FINISH = Character.toString(127_937) + " ";
    private Util util = new Util();
    private Player player = new Player(util.numberRandom(), util.numberRandom());
    private Printer printer = new Printer();
    private Obstacles obstacles = new Obstacles();

    public void mapGenerator() {

        fillMap();

        obstacles.generatorObstacles(mapGame);

        player.generatorPlayer(mapGame);

        printMap();
    }

    public void fillMap() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                mapGame[i][j] = WAY;
            }
        }

        mapGame[9][9] = FINISH;

    }

    public void printMap(){
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                printer.printerMapSections(mapGame[i][j]);
            }
            printer.lineBreak();
        }
    }

    public String[][] getMapGame() {
        return mapGame;
    }
    
    public static String getWay() {
        return WAY;
    }

    public static String getFinish() {
        return FINISH;
    }

    public Player getPlayer() {
        return player;
    }

}