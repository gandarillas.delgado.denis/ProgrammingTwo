package TASKSEVEN;

public class Message{

    private Printer printer = new Printer();

    public void title(){
        printer.printMessageTitle("ESCAPE THE ROOM");
    }

    public void enterMovements(){
        printer.printMessage("INTRODUCE MOVEMENT:");
    }

    public void messageErrorMove(){
        printer.printMessageError("Movement cannot be performed");
    }

    public void messageFinish(){
        printer.printFinish("YOU WIN!!!");
    }

}