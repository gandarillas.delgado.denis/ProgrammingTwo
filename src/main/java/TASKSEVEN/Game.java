package TASKSEVEN;

public class Game {

    private GameMap gameMap = new GameMap();
    private boolean stopGame = false;
    private Printer printer = new Printer();
    private Message message = new Message();

    public void playGame() {

        message.title();

        printer.printAvailableKeys('W', "Move up");
        printer.printAvailableKeys('S', "Move down");
        printer.printAvailableKeys('A', "Move left");
        printer.printAvailableKeys('D', "Move right");

        gameMap.mapGenerator();

        while (!stopGame) {

            int x = gameMap.getPlayer().getXPosition();
            int y = gameMap.getPlayer().getyPosition();

            printer.printCoords(x, y);

            gameMap.getPlayer().movePlayer(gameMap.getMapGame());
            gameMap.printMap();

            if (gameMap.getPlayer().getXPosition() == 9 && gameMap.getPlayer().getyPosition() == 9) {
                stopGame = true;
            }

        }

        message.messageFinish();

    }

}
