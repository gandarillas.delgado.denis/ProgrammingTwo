package TASKSEVEN;

public class Obstacles {
    private static final String OBSTACLES = Character.toString(128_520) + " ";

    public void generatorObstacles(String[][] mapGame) {
        int xPosition = (int) (Math.random() * 8 + 1);
        int yPosition = (int) (Math.random() * 8 + 1);
        for (int i = 0; i < 5; i++) {

            if (mapGame[yPosition][xPosition] == "*  ") {
                mapGame[yPosition][xPosition] = OBSTACLES;

                xPosition = (int) (Math.random() * 9 + 1);
                yPosition = (int) (Math.random() * 9 + 1);
            }
            if (mapGame[yPosition][xPosition] != "*  ") {
                i--;
                xPosition = (int) (Math.random() * 9 + 1);
                yPosition = (int) (Math.random() * 9 + 1);
            }

        }
    }

    public static String getObstacles() {
        return OBSTACLES;
    }

}
