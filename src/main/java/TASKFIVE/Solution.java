package TASKFIVE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {

    public static int sumGroups(int[] arr) {

        int sizeArray = 0;
        int index = 0;
        int indexAux = 0;

        if (arr.length > 0) {
            List<Integer> listFinal = new ArrayList<>(Arrays.asList(arr[index]));
            for (int i = 1; i < arr.length; i++) {
                if (arr[i - 1] % 2 == arr[i] % 2) {
                    listFinal.set(indexAux, listFinal.get(indexAux) + arr[i]);
                } else {
                    listFinal.add(arr[i]);
                    indexAux++;
                }
            }
            int[] arrayFinal = listFinal.stream().mapToInt(i -> i).toArray();

            if (arrayFinal.length == arr.length){
                return arrayFinal.length;
            }
            return sumGroups(arrayFinal);

        }

        return sizeArray;
    }

    public static void main(String[] args) {
        System.out.println(sumGroups(new int[] { 2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9 }));
        System.out.println(6);
    }
}