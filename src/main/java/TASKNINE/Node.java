package TASKNINE;

public class Node{
    public int data;
    public Node next = null;
    
    public static int getNth(Node n, int index) throws Exception{

        int result = 0;
        if (index != 0) {
            result = getNth(n.next, index - 1);
        }else{
            result = n.data;
        }

        return result;
    }
}