package TASKNINE;

public class AppendNode {

    public int data;
    public Node next = null;

    public AppendNode(final int data) {
        this.data = data;
    }

    public AppendNode(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    public static Node append(Node listA, Node listB) {

        if (listA == null) {
            listA = listB;
        } else {
            Node auxiliar = listA;

            while (auxiliar.next != null) {
                auxiliar = auxiliar.next;
            }

            auxiliar.next = listB;
        }

        return listA;
    }
}