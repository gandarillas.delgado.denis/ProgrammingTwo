package TASKONE;

public class People {
	private int age;
	private String name, lastName, city, job;
	private static final String GREET = "hello";

	public People(int age, String name, String lastName, String city, String job) {
		this.age = age;
		this.name = name;
		this.lastName = lastName;
		this.city = city;
		this.job = job;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getCity() {
		return city;
	}

	public String getJob() {
		return job;
	}

	public static String getGreet() {
		return GREET;
	}

	public String greet() {
		return GREET + " my name is " + name;
	}

	@Override
	public String toString() {
		return "People [age=" + age + ", city=" + city + ", job=" + job + ", lastName=" + lastName + ", name=" + name
				+ "]";
	}

}