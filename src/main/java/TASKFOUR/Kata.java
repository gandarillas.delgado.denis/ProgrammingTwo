package TASKFOUR;

public class Kata {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static int mostFrequentItemCount(int[] collection) {

        int count = 0;

        if (collection.length > 0) {

            int countAuxiliar = 0;

            for (int i = 0; i < collection.length; i++) {
                int auxiliar = collection[i];
                for (int j = 0; j < collection.length; j++) {
                    if (collection[j] == auxiliar) {
                        countAuxiliar++;
                    }

                    if (countAuxiliar > count) {
                        count = countAuxiliar;
                    }

                }
                countAuxiliar = 0;
            }

        }

        return count;

    }

    public static void main(String[] args) {

        System.out.println(ANSI_WHITE_BACKGROUND+ANSI_RED+"====================");
        System.out.print("RESULT: ");
        System.out.println(Kata.mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
        System.out.println("===================="+ANSI_RESET);
    }

}