package TASKSIX;

public class Rotator {

    public Object[] rotate(Object[] data, int n) {

        boolean firstChangeCheck = false;
        Object auxiliar = 0;
        Object variableAuxiliar = 0;

        if (n > 0) {

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < data.length - 1; j++) {

                    if (auxiliar.equals(0)) {
                        auxiliar = data[j];
                    }

                    if (!firstChangeCheck) {
                        data[0] = data[data.length - 1];
                        firstChangeCheck = true;
                    }

                    variableAuxiliar = data[j + 1];

                    data[j + 1] = auxiliar;

                    auxiliar = variableAuxiliar;

                }
                firstChangeCheck = false;
            }

        }

        if (n < 0) {

            for (int i = n; i < 0; i++) {
                for (int j = data.length; j > 0; j--) {

                    if (auxiliar.equals(0)) {
                        auxiliar = data[j - 1];
                    }

                    if (!firstChangeCheck) {
                        data[j - 1] = data[0];
                        firstChangeCheck = true;
                    }

                    if (j > 1) {
                        variableAuxiliar = data[j - 2];
                        data[j - 2] = auxiliar;

                        auxiliar = variableAuxiliar;
                    }

                }
                firstChangeCheck = false;
            }

        }

        return data;
    }

}