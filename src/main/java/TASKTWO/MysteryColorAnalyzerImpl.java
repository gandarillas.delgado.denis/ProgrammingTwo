package TASKTWO;

import java.util.List;

public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer {

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        int totalColors = (int) mysteryColors.stream().distinct().count();
        return totalColors;
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int totalColors = 0;

        for (int index = 0; index < mysteryColors.size(); index++) {
            if (mysteryColors.get(index).equals(color)) {
                totalColors++;
            }
        }

        return totalColors;
    }
    
}
