package TASKFOURTEEN;

import TASKFOURTEEN.recursividad.Contador;
import TASKFOURTEEN.recursividad.Infinito;
import TASKFOURTEEN.recursividad.LlamadasEncadenadas;
import TASKFOURTEEN.recursividad.RecursividadvsIteracion;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo - LISTO
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour - LISTO
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion:
        //  El problema es que dicho metodo no realiza ninguna accion aparte de llamarse por si solo, ademas de no devolver ningun resultado.
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion:
        //  Dicho metodo realiza una cuenta hacia atras desde el numero ingresado, mediante la recursividad, 
        //  en donde se llama nuevamente a dicho metodo, pero se resta (-1) al valor introducido.

        Contador.contarHasta(10);

        System.out.println("--------------------------------");

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}
