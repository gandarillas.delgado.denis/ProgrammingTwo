package TASKEIGHT;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JUArrayList implements List<Integer> {

    private int[] numbers = new int[10];
    private int countAdd = 0;

    // Obligatory methods

    public JUArrayList() {

    }

    @Override
    public int size() {
        return numbers.length;
    }

    @Override
    public boolean isEmpty() {

        return numbers.length == 1;
    }

    @Override
    public Iterator<Integer> iterator() {

        /*
         * int elementIterator = 0;
         * 
         * if (numbers.length > 0) {
         * elementIterator = numbers[1];
         * }
         */

        return null;
    }

    @Override
    public boolean add(Integer e) {
        boolean verifyAdd = false;
        if (countAdd >= numbers.length) {
            int[] arrayAuxiliar = new int[numbers.length + 1];

            for (int i = 0; i < numbers.length; i++) {
                arrayAuxiliar[i] = numbers[i];
            }

            arrayAuxiliar[countAdd] = e;

            numbers = arrayAuxiliar;

            countAdd++;
        }

        if (countAdd < numbers.length && verifyAdd == false) {
            numbers[countAdd] = e;
            verifyAdd = true;
            countAdd++;
        }

        return verifyAdd;
    }

    @Override
    public boolean remove(Object o) {

        boolean verifyElement = false;

        int elementSearch = (int) o;
        int countSearch = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == elementSearch && countSearch < 1) {
                verifyElement = true;
                countSearch++;
            }
            if (verifyElement && i < numbers.length - 1) {
                numbers[i] = numbers[i + 1];
            }
        }

        return verifyElement;
    }

    @Override
    public void clear() {
        int[] arrayClear = new int[1];
        numbers = arrayClear;
        countAdd = 0;
    }

    @Override
    public Integer get(int index) {

        int elementPosition = 0;

        if (index >= 0 && index < numbers.length) {
            elementPosition = numbers[index];
        }

        return elementPosition;
    }

    @Override
    public Integer remove(int index) {
        int[] arrayRemove = new int[numbers.length - 1];
        int elementRemove = numbers[index];

        for (int i = 0; i < arrayRemove.length; i++) {
            if (i < index) {
                arrayRemove[i] = numbers[i];
            }
            if (i >= index) {
                arrayRemove[i] = numbers[i + 1];
            }
        }

        numbers = arrayRemove;

        return elementRemove;
    }

    // ------------------------------------------------------------------------------------------
    // Optional methods

    @Override
    public boolean contains(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Integer set(int arg0, Integer arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void add(int arg0, Integer arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // TODO Auto-generated method stub
        return null;
    }

    // ------------------------------------------------------------------------------------------
    // Methods not required

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean addAll(int arg0, Collection<? extends Integer> arg1) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int indexOf(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<Integer> subList(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return null;
    }

}
