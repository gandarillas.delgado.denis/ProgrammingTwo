package TASKTEN;

public class Nodo<T> {
    public T data;
    public Nodo<T> next;

    public Nodo(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Nodo [data=" + data + ", next=" + next + "]";
    }

}
