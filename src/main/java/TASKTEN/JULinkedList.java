package TASKTEN;

import java.util.Iterator;
import java.util.List;

public class JULinkedList<T> implements Lista<T> {

    // -------------------------------------------------
    // Attributes

    private Nodo<T> raiz;
    private int countSize = 0;

    // -------------------------------------------------
    // Obligatory methods

    @Override
    public int size() {
        return countSize;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<T> iterator() {
        
        return new Iterator<>() {
            int count = 0;
            Nodo<T> nodo = raiz;
            public boolean hasNext() {
                return size() > count;
            }
            public T next() {
                nodo = nodo.next;
                count++;
                return nodo.data;
            }
        };
    }

    @Override
    public boolean add(T e) {

        Nodo<T> auxiliarNodo = new Nodo<>(e);

        if (isEmpty()) {
            raiz = auxiliarNodo;
            countSize++;
        } else {

            Nodo<T> lastElemenNodo = nodoGet(countSize - 1);

            lastElemenNodo.next = auxiliarNodo;

            countSize++;
        }

        return countSize > 0;
    }

    @Override
    public boolean remove(Object o) {

        Nodo<T> auxNodo = raiz;
        Nodo<T> nodo = raiz;
        int sizeAux = countSize;

        while (nodo != null) {
            if (nodo.data == o) {
                if (raiz == nodo) {
                    raiz = nodo.next;
                } else {
                    auxNodo.next = nodo.next;
                }
                countSize--;
                break;
            }
            auxNodo = nodo;
            nodo = nodo.next;
        }

        return countSize < sizeAux;
    }

    @Override
    public void clear() {

        raiz = null;
        countSize = 0;

    }

    @Override
    public T get(int index) {

        return nodoGet(index).data;
    }

    @Override
    public T remove(int index) {

        T element = null;

        if (!isEmpty() && index < countSize) {
            Nodo<T> nodo = nodoGet(index);

            element = nodo.data;

            nodo.data = nodoGet(index + 1).data;

            nodo.next = nodoGet(index + 1).next;

            countSize--;
        }

        return element;
    }

    @Override
    public boolean contains(Object o) {

        int quantityElements = 0;

        for (int i = 0; i < countSize; i++) {

            if (nodoGet(i).data == o) {

                quantityElements++;

            }

        }

        return quantityElements > 0;
    }

    @Override
    public T set(int index, T elementReplace) {
        T element = null;

        if (!isEmpty() && index < countSize) {

            Nodo<T> nodo = nodoGet(index);
            element = nodo.data;
            nodo.data = elementReplace;

        }

        return element;
    }

    @Override
    public int indexOf(Object o) {
        int position = 0;
        int positionVerify = 0;

        for (int i = 0; i < countSize; i++) {

            if (nodoGet(i).data == o) {
                i = countSize;
                positionVerify = position;
            }

            position++;

        }

        return positionVerify;
    }

    @Override
    public int lastIndexOf(Object o) {

        int count = countSize - 1;
        int position = countSize;

        while (count >= 0) {
            if (nodoGet(count).data == o) {
                count = -1;
            } else {
                position--;
                count--;
            }
        }

        return position;
    }

    // -------------------------------------------------
    // Optional methods

    @Override
    public void add(int index, T element) {
        //TODO Auto-generated method stub

    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<T> subList(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    // -------------------------------------------------
    // Own methods

    public Nodo<T> nodoGet(int index) {

        Nodo<T> nodoAux = raiz;
        Nodo<T> element = null;

        if (!isEmpty() && index < countSize) {
            for (int i = 0; i < countSize; i++) {
                if (i == index) {
                    element = nodoAux;
                }
                nodoAux = nodoAux.next;
            }
        }

        return element;
    }

}
