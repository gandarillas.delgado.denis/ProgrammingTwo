package TASKTEN;

import java.util.Iterator;
import java.util.List;

public interface Lista<T> {

    int size();

    boolean isEmpty();

    Iterator<T> iterator();

    boolean add(T element);

    boolean remove(Object o);

    void clear();

    T get(int index);

    T remove(int index);

    boolean contains(Object o);

    T set(int index, T elementReplace);

    int indexOf(Object o);

    int lastIndexOf(Object o);

    // -------------------------------------------------
    // Optional methods

    void add(int index, T Element);

    Object[] toArray();

    <T> T[] toArray(T[] a);

    List<T> subList(int arg0, int arg1);

}
