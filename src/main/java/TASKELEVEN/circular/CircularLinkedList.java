package TASKELEVEN.circular;

import TASKELEVEN.List;

class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() { return "data=" + data; }
}
public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {

        Node<T> nodo = new Node<>(data);

        if (isEmpty()) {
            head = nodo;
            head.setNext(head);
            size++;
        }else{

            Node<T> nodoAux = nodoGet(size - 1);

            nodoAux.setNext(nodo);
            nodoAux.getNext().setNext(head);
            
            size++;
        }


        return true;
    }

    // -------------------------------------------------
    // Own methods

    public Node<T> nodoGet(int index) {

        Node<T> nodoAux = head;
        Node<T> element = null;

        if (!isEmpty() && index < size) {
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    element = nodoAux;
                }
                nodoAux = nodoAux.getNext();
            }
        }

        return element;
    }
    // -------------------------------------------------

    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }

    public static void main(String[] args) {
        List<Integer> list = new CircularLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
    }
}
