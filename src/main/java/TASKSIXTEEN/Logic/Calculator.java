package TASKSIXTEEN.Logic;

import TASKSIXTEEN.Messages.OperationException;
import TASKSIXTEEN.Messages.VerifierException;

public class Calculator {

    private Verifier verifier = new Verifier();
    private Separator separator = new Separator();
    private Creator creator = new Creator();
    private Search search = new Search();
    private Operations operations = new Operations();
    private Changer changer = new Changer();
    private StackList<Character> operationList;
    private String operationString;
    private StackList<Integer> numbers;
    private StackList<String> listOperation;
    private StackList<String> auxList;
    private int resultBrackets;
    private String operationBrackets;
    private StackList<String> operationBracketsList;
    private String listAux;
    private String operationBracketsAux;

    public int calculator(String operation) throws VerifierException, OperationException{
        int result = 0;
        operationList = separator.separator(operation);
        numbers = creator.creatorNumbers(operationList);
        verifier.verifierGeneral(operation, numbers);
        operationString = creator.createOperation(operationList);
        listOperation = separator.separatorString(operationString);
        result = calculatorOperations();
        return result;
    }

    public int calculatorOperations() throws OperationException{
        int result = 0;
        while (search.searchBrackets(listOperation)) {
            calculatorBrackets(operationList);
            changeOperationBrackets();
        }
        changeOperation();
        operationsNotBrackets();
        result = Integer.parseInt(operationString);
        return result;
    }

    public void changeOperationBrackets(){
        operationString = creator.createOperation(operationList);
        if (!verifier.verifyNotBrackets(operationString)) listOperation = separator.separatorString(operationString);
        else{
            StackList<String> auxList = separator.separatorString(operationString);
            listOperation = auxList;
        }
    }
    
    public void changeOperation(){
        String resultString = operationString;
        auxList = separator.separatorString(resultString);
        listOperation = auxList;
    }

    public void operationsNotBrackets() throws OperationException{
        while (search.searchFactorials(listOperation)) {
            calculatorFactorial(listOperation);
            changeOperation();
        }
        while (search.searchPows(listOperation)) {
            calculatorPow(listOperation);
            changeOperation();
        }
        while (search.searchAdditions(listOperation)) {
            calculatorAddition(listOperation);
            changeOperation();
        }
        while (search.searchMultiplications(listOperation)) {
            calculatorMultiplication(listOperation);
            changeOperation();
        }
    }

    public int calculatorBrackets(StackList<Character> operation) throws OperationException{
        int result = 0;
        operationBrackets = creator.createOperationBrackets(operation);
        operationBracketsList = separator.separatorString(operationBrackets);
        listAux = creator.createOperation(operation);
        operationBracketsAux = operationBrackets;
        result = operationWithBrackets(operation);
        return result;
    }

    public int operationWithBrackets(StackList<Character> operation) throws OperationException{
        resultBrackets = 0;
        while (search.searchBrackets(operationBracketsList)) {
            char operator = verifier.operationToPerform(operationBrackets); 
            allOperationsBrackets(operator, operation);
        }
        return resultBrackets;
    }

    public void allOperationsBrackets(char operator, StackList<Character> operation) throws OperationException{
        switch (operator) {
            case '!':
                factorialOperation();
                break;
            case '^':
                powOperation();
                break;
            case '+':
                addOPeration();
                break;
            case '*':
                multiplicationOperation();
                break;
            default:
                notOperation(operation);
                break;
        }
    }

    public void notOperation(StackList<Character> operation){
        resultBrackets = Integer.parseInt(operationBracketsList.getStack(1).getData());
        operationBracketsList = changer.removeBrackets(operationBracketsList);
        int position = search.searchPositionBrackets(operation);
        operationList = changer.changeOperation(listAux, operationBracketsAux, resultBrackets, position);
    }

    public void factorialOperation() throws OperationException{
        operationBrackets = calculatorFactorial(operationBracketsList);
        operationBracketsList = separator.separatorString(operationBrackets);
    }

    public void powOperation() throws OperationException{
        operationBrackets = calculatorPow(operationBracketsList);
        operationBracketsList = separator.separatorString(operationBrackets);
    }

    public void addOPeration() throws OperationException{
        operationBrackets = calculatorAddition(operationBracketsList);
        operationBracketsList = separator.separatorString(operationBrackets);
    }

    public void multiplicationOperation() throws OperationException{
        operationBrackets = calculatorMultiplication(operationBracketsList);
        operationBracketsList = separator.separatorString(operationBrackets);
    }

    public String calculatorFactorial(StackList<String> operation) throws OperationException{
        int position = search.searchPositionOperator('!', operation);
        int number = Integer.parseInt(operation.getStack(position - 1).getData());
        int result = operations.factorialOperation(number);
        operation = changer.changeOperationFactorialByResult(operation, result, position);
        operationString = creator.createOperationString(operation);
        return operationString;
    }

    public String calculatorPow(StackList<String> operation) throws OperationException{
        int position = search.searchPositionOperator('^', operation);
        int number = Integer.parseInt(operation.getStack(position - 1).getData());
        int pow = Integer.parseInt(operation.getStack(position + 1).getData());
        int result = operations.powOperation(number, pow);
        operation = changer.changeOperationTwoNumbersByResult(operation, result, position);
        operationString = creator.createOperationString(operation);
        return operationString;
    }

    public String calculatorAddition(StackList<String> operation) throws OperationException{
        int position = search.searchPositionOperator('+', operation);
        int firstNumber = Integer.parseInt(operation.getStack(position - 1).getData());
        int lastNumber = Integer.parseInt(operation.getStack(position + 1).getData());
        int result = operations.additionOperation(firstNumber, lastNumber);
        operation = changer.changeOperationTwoNumbersByResult(operation, result, position);
        operationString = creator.createOperationString(operation);
        return operationString;
    }

    public String calculatorMultiplication(StackList<String> operation) throws OperationException{
        int position = search.searchPositionOperator('*', operation);
        int firstNumber = Integer.parseInt(operation.getStack(position - 1).getData());
        int lastNumber = Integer.parseInt(operation.getStack(position + 1).getData());
        int result = operations.multiplicationOperation(firstNumber, lastNumber);
        operation = changer.changeOperationTwoNumbersByResult(operation, result, position);
        operationString = creator.createOperationString(operation);
        return operationString;
    }

}