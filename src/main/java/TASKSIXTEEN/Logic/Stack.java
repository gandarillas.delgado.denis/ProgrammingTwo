package TASKSIXTEEN.Logic;

public class Stack<T extends Comparable<T>> {

    private T data;
    private Stack<T> next;

    public Stack(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Stack<T> getNext() {
        return next;
    }

    public void setNext(Stack<T> next) {
        this.next = next;
    }
}