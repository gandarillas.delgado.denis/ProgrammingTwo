package TASKSIXTEEN.Logic;

public class Separator {

    private String element;
    private StackList<String> operationList;

    public StackList<Character> separator(String operation) {
        StackList<Character> operationStack = new StackList<>();
        for (int i = 0; i < operation.length(); i++) {
            operationStack.push(operation.charAt(i));
        }
        return operationStack;
    }

    public StackList<String> separatorString(String operation) {
        operationList = new StackList<>();
        element = "";
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) >= '0' && operation.charAt(i) <= '9') {
                if ((i + 1) < operation.length()) {
                    if (operation.charAt(i + 1) == '+' || operation.charAt(i + 1) == '*' || operation.charAt(i + 1) == '^' || operation.charAt(i + 1) == '!' || operation.charAt(i + 1) == ')' || operation.charAt(i + 1) == '(') {
                        addElement(operation, i);
                    } else element = element + operation.charAt(i);
                }else{
                    addElement(operation, i);
                }
            } else {
                addElement(operation, i);
            }
        }
        return operationList;
    }

    public void addElement(String operation, int index){
        element = element + operation.charAt(index);
        operationList.push(element);
        element = "";
    }

}
