package TASKSIXTEEN.Logic;

import TASKSIXTEEN.Messages.VerifierException;

public class Verifier {

    public boolean verifyRange(int number) {
        int numberLastRange = 2147483647;
        boolean verifyRange = false;
        if (number >= 0 && number <= numberLastRange)
            verifyRange = true;
        return verifyRange;
    }

    public boolean verifyTypeVariable(String operation) {
        boolean verifyTypeVariableInt = true;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '.' || operation.charAt(i) == ',')
                verifyTypeVariableInt = false;
        }
        return verifyTypeVariableInt;
    }

    public boolean verifyBrackets(String operation) {
        boolean verifyBrackets = false;
        int countFirstBracket = 0, countLastBracket = 0;
        char element, elementNext, elementBefore;
        for (int i = 0; i < operation.length(); i++) {
            element = operation.charAt(i);
            if (i == operation.length()) elementNext = operation.charAt(i + 1);
            else elementNext = ' ';
            if (element == '(' && elementNext != ')') verifyBrackets = true;
            if (i > 0) elementBefore = operation.charAt(i - 1);
            else elementBefore = ' ';
            if (element == ')' && elementBefore != '(') verifyBrackets = true;
            if (element == '(') countFirstBracket++;
            if (element == ')') countLastBracket++;
        }
        if (operation.charAt(0) == ')')verifyBrackets = false;
        if (countFirstBracket == countLastBracket) verifyBrackets = true;
        else verifyBrackets = false;
        return verifyBrackets;
    }

    public boolean verifyOperationNotPermitted(String operation){
        boolean verifyOperation = false;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '/' || operation.charAt(i) == '-') {
                verifyOperation = true;
                i = operation.length();
            }
        }
        return verifyOperation;
    }

    public boolean verifyNotLetters(String operation){
        boolean verifyLetters = false;
        for (int i = 0; i < operation.length(); i++) {
            if ((operation.charAt(i) >= 'a' && operation.charAt(i) <= 'z') || (operation.charAt(i) >= 'A' && operation.charAt(i) <= 'Z')) {
                verifyLetters = true;
                i = operation.length();
            }
        }
        return verifyLetters;
    }

    public boolean verifyPositionOperator(String operator){
        boolean verifyOperator = true;
        char element;
        for (int i = 0; i < operator.length(); i++) {
            element = operator.charAt(i);
            if (element == '+' || element == '*'){
                if ((operator.charAt(i + 1) >= 0 && operator.charAt(i + 1) <= '9') && (i == 0 || operator.charAt(i - 1) == '(')) {
                    verifyOperator = false;
                }
            }
        }
        return verifyOperator;
    }

    public boolean verifyResultOperation(int result){
        boolean verifyResult = true;
        if (result <= 0) {
            verifyResult = false;
        }
        return verifyResult;
    }

    public void verifierGeneral(String operation, StackList<Integer> numbers) throws VerifierException{
        if (!verifyBrackets(operation)) throw new VerifierException("The entered operation is missing a parenthesis.", operation);
        if (verifyOperationNotPermitted(operation)) throw new VerifierException("Your operation involves unsupported operators.\n\t\tOperators not permitted: - and /", operation);
        if (!verifyPositionOperator(operation)) throw new VerifierException("In this operation there is an operand in a bad position.", operation);
        if (verifyNotLetters(operation)) throw new VerifierException("Operations cannot contain letters.", operation);
        for (int i = 0; i < numbers.size(); i++) {
            if (!verifyRange(numbers.getStack(i).getData())) throw new VerifierException("Number entered outside the allowed range.");
        }
        if (!verifyTypeVariable(operation)) throw new VerifierException("The entered operation contains decimals.", operation);
    }

    public char operationToPerform(String operation){
        char operator = 'x';
        if (verifyOperatorFactorial(operation)) operator = '!';
        else if (verifyOperatorPow(operation)) operator = '^';
            else if (verifyOperatorAddition(operation)) operator = '+';
                else if (verifyOperatorMultiplication(operation)) operator = '*';
        return operator;
    }

    public boolean verifyOperatorFactorial(String operation){
        boolean verifyOperator = false;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '!') {
                verifyOperator = true;
                i = operation.length();
            }
        }
        return verifyOperator;
    }

    public boolean verifyOperatorPow(String operation){
        boolean verifyOperator = false;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '^') {
                verifyOperator = true;
                i = operation.length();
            }
        }
        return verifyOperator;
    }

    public boolean verifyOperatorAddition(String operation){
        boolean verifyOperator = false;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '+') {
                verifyOperator = true;
                i = operation.length();
            }
        }
        return verifyOperator;
    }

    public boolean verifyOperatorMultiplication(String operation){
        boolean verifyOperator = false;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '*') {
                verifyOperator = true;
                i = operation.length();
            }
        }
        return verifyOperator;
    }

    public boolean verifyNotBrackets(String operation){
        boolean verifyNotBrackets = true;
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '(' || operation.charAt(i) == ')') {
                verifyNotBrackets = false;
            }
        }
        return verifyNotBrackets;
    }

}