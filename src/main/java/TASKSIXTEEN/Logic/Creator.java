package TASKSIXTEEN.Logic;

public class Creator {

    private int numberFinal;
    private String numbersList;
    private StackList<Integer> listNumbers;

    public StackList<Integer> creatorNumbers(StackList<Character> operation) {
        listNumbers = new StackList<>();
        numbersList = "";
        numberFinal = 0;
        for (int i = 0; i < operation.size(); i++) {
            if (operation.getStack(i).getData() >= '0' && operation.getStack(i).getData() <= '9') {
                numbersList = numbersList + operation.getStack(i).getData();
            } else {
                if (numbersList != "") {
                    addNumber();
                }
                numbersList = "";
            }
        }
        if (numbersList != "") {
            addNumber();
        }
        return listNumbers;
    }

    public void addNumber(){
        numberFinal = Integer.parseInt(numbersList);
        listNumbers.push(numberFinal);
    }

    public String createOperation(StackList<Character> listOperation) {
        String operation = "";
        for (int i = 0; i < listOperation.size(); i++) {
            operation = operation + listOperation.getStack(i).getData();
        }
        return operation;
    }

    public String createOperationString(StackList<String> listOperation) {
        String operation = "";
        for (int i = 0; i < listOperation.size(); i++) {
            operation = operation + listOperation.getStack(i).getData();
        }
        return operation;
    }

    public String createOperationBrackets(StackList<Character> operation) {
        String operationBrackets = "";
        for (int i = 0; i < operation.size(); i++) {
            if (operation.getStack(i).getData() == '(' && operation.getStack(i + 1).getData() != '(') {
                for (int j = i; j < operation.size(); j++) {
                    if (operation.getStack(j).getData() != ')') operationBrackets = operationBrackets + operation.getStack(j).getData();
                    if (operation.getStack(j).getData().equals('(')) {
                        operationBrackets = "";
                        operationBrackets = operationBrackets + operation.getStack(j).getData();
                    } 
                    if (operation.getStack(j).getData().equals(')')) {
                        operationBrackets = operationBrackets + operation.getStack(j).getData();
                        i = operation.size();
                        j = operation.size();
                    }
                }
            }
        }
        return operationBrackets;
    }

    public String createOperationTwoNumbers(StackList<Character> operationList){
        String operation = "";
        for (int i = 0; i < operationList.size(); i++) {
            if ((operationList.getStack(i).getData().equals('(')) || (operationList.getStack(i).getData().equals(')')));
            else operation = operation + operationList.getStack(i).getData();
        }
        return operation;
    }

}
