package TASKSIXTEEN.Logic;

import TASKSIXTEEN.Messages.OperationException;

public class Operations {

    private Verifier verifier = new Verifier();

    public int additionOperation(int firtsNumber, int lastNumber) throws OperationException {
        int resultAddition = firtsNumber + lastNumber;
        if (!verifier.verifyResultOperation(resultAddition))
            throw new OperationException("The addition result is greater than the allowable range.", firtsNumber,
                    lastNumber, '+');
        return resultAddition;
    }

    public int multiplicationOperation(int firtsNumber, int lastNumber) throws OperationException {
        int resultMultiplication = firtsNumber * lastNumber;
        if (!verifier.verifyResultOperation(resultMultiplication))
            throw new OperationException("The multiplication result is greater than the allowable range.", firtsNumber,
                    lastNumber, '*');
        return resultMultiplication;
    }

    public int factorialOperation(int number) throws OperationException {
        int resultFactorial = 1;
        int numberAux = number;
        while (number > 0) {
            resultFactorial = resultFactorial * number;
            number--;
        }
        if (!verifier.verifyResultOperation(resultFactorial))
            throw new OperationException("The factorial result is greater than the allowable range.", numberAux, '!');
        return resultFactorial;
    }

    public int powOperation(int number, int pow) throws OperationException {
        int resultPow = 1;
        for (int i = 0; i < pow; i++) {
            resultPow = resultPow * number;
        }
        if (!verifier.verifyResultOperation(resultPow))
            throw new OperationException("The power result is greater than the allowable range.", number, pow, '^');
        return resultPow;
    }

}