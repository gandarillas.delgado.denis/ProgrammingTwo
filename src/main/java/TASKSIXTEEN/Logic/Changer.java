package TASKSIXTEEN.Logic;

public class Changer{

    private Separator separator = new Separator();

    public String changeOperationByResult(String operation, int result, int sizeOperationPerformed){
        String operationChanges = "";
        if (operation.charAt(0) == '(') {
            operationChanges = operationChanges + operation.charAt(0);
            sizeOperationPerformed++;
        }
        operationChanges = operationChanges + result;
        for (int i = sizeOperationPerformed; i < operation.length(); i++) {
            operationChanges = operationChanges + operation.charAt(i);
        }
        return operationChanges;
    }

    public StackList<String> changeOperationFactorialByResult(StackList<String> factorial, int result, int position){
        StackList<String> operationChange = new StackList<>();
        for (int i = 0; i < factorial.size(); i++) {
            if (i == (position - 1)) {
                String resultString = ""+result;
                operationChange.push(resultString);
                i = i + 1;
            }else operationChange.push(factorial.getStack(i).getData());
        }
        return operationChange;
    }

    public StackList<String> changeOperationTwoNumbersByResult(StackList<String> twoNumbers, int result, int position){
        StackList<String> operationChange = new StackList<>();
        for (int i = 0; i < twoNumbers.size(); i++) {
            if (i == (position - 1)) {
                String resultString = ""+result;
                operationChange.push(resultString);
                i = i + 2;
            }else operationChange.push(twoNumbers.getStack(i).getData());
        }
        return operationChange;
    }

    public StackList<String> changeOperationByResult(StackList<String> operation, int result, int sizeOperationPerformed){
        StackList<String> operationChanges = new StackList<>();
        if (operation.getStack(0).getData().equals("(")) {
            operationChanges.push(operation.getStack(0).getData());
            sizeOperationPerformed++;
        }
        operationChanges.push(""+result);
        for (int i = sizeOperationPerformed; i < operation.size(); i++) {
            operationChanges.push(operation.getStack(0).getData());
        }
        return operationChanges;
    }

    public StackList<String> removeBrackets(StackList<String> operation){
        StackList<String> removeList = new StackList<>();
        for (int i = 0; i < operation.size(); i++) {
            if (operation.getStack(i).getData().equals("(") || operation.getStack(i).getData().equals(")") );
            else removeList.push(operation.getStack(i).getData());
        }
        return removeList;
    }

    public StackList<Character> changeOperation(String operation, String operationBrackets, int result, int position){
        StackList<Character> changeOperationList = new StackList<>();
        StackList<Character> operationList = separator.separator(operationBrackets);
        for (int i = 0; i < operation.length(); i++) {
            if (i == position) {
                int counter = 0;
                while (operationList != null) {
                    String resultString = ""+result;
                    changeOperationList.push(resultString.charAt(counter));
                    if (counter == resultString.length() - 1) {
                        i = i + (operationList.size() - 1);
                        operationList = null;
                    }
                    counter++;
                }
            }else{
                changeOperationList.push(operation.charAt(i));
            }
        }
        return changeOperationList;
    }
}