package TASKSIXTEEN.Logic;

public class StackList<T extends Comparable<T>> {

    private Stack<T> raiz;
    private int size = 0;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return raiz == null;
    }

    public T push(T element) {
        Stack<T> stackAux = new Stack<T>(element);
        if (size == 0) {
            raiz = stackAux;
            size++;
        } else {
            Stack<T> lastStack = getStack(size - 1);
            lastStack.setNext(stackAux);
            size++;
        }
        return element;
    }

    public T pop() {
        T elementRemove = null;
        if (size == 1) {
            elementRemove = raiz.getData();
            raiz = null;
            size = 0;
        }
        if (size >= 2) {
            Stack<T> stackAux = getStack(size - 2);
            elementRemove = stackAux.getNext().getData();
            stackAux.setNext(null);
            size--;
        }
        return elementRemove;
    }

    public Stack<T> getStack(int index) {
        Stack<T> stackAux = raiz;
        Stack<T> element = null;
        if (!(size == 0) && index < size) {
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    element = stackAux;
                }
                stackAux = stackAux.getNext();
            }
        }
        return element;
    }

}
