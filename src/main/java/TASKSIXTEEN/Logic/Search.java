package TASKSIXTEEN.Logic;

public class Search {
    
    public boolean searchBrackets(StackList<String> operationBracketsList){
        boolean bracketFound = false;
        int counterBrackets = 0;
        for (int i = 0; i < operationBracketsList.size(); i++) {
            if (operationBracketsList.getStack(i).getData().equals("(")) 
                counterBrackets++;
        }
        if (counterBrackets > 0) 
            bracketFound = true;
        return bracketFound;
    }

    public int searchPositionBrackets(StackList<Character> operation){
        int position = 0;
        for (int i = 0; i < operation.size(); i++) {
            if (operation.getStack(i).getData().equals('(')) {
                for (int j = i; j < operation.size(); j++) {
                    if (operation.getStack(j).getData().equals('(')) position = j;
                }
            }
        }
        return position;
    }

    public int searchPositionOperator(char operator, StackList<String> operation){
        int position = 0;
        String operatorString = ""+operator;
        for (int i = 0; i < operation.size(); i++) {
            if (operation.getStack(i).getData().equals(operatorString)) {
                position = i;
                i = operation.size();
            }
        }
        return position;
    }

    public boolean searchFactorials(StackList<String> operationBracketsList){
        boolean factorialFound = false;
        int counterFactorials = 0;
        for (int i = 0; i < operationBracketsList.size(); i++) {
            if (operationBracketsList.getStack(i).getData().equals("!")) 
                counterFactorials++;
        }
        if (counterFactorials > 0) 
            factorialFound = true;
        return factorialFound;
    }

    public boolean searchPows(StackList<String> operationBracketsList){
        boolean powFound = false;
        int counterPow = 0;
        for (int i = 0; i < operationBracketsList.size(); i++) {
            if (operationBracketsList.getStack(i).getData().equals("^")) 
                counterPow++;
        }
        if (counterPow > 0) 
            powFound = true;
        return powFound;
    }

    public boolean searchAdditions(StackList<String> operationBracketsList){
        boolean addFound = false;
        int counterAdd = 0;
        for (int i = 0; i < operationBracketsList.size(); i++) {
            if (operationBracketsList.getStack(i).getData().equals("+")) 
                counterAdd++;
        }
        if (counterAdd > 0) 
            addFound = true;
        return addFound;
    }

    public boolean searchMultiplications(StackList<String> operationBracketsList){
        boolean multiplicationFound = false;
        int counterMultiplication = 0;
        for (int i = 0; i < operationBracketsList.size(); i++) {
            if (operationBracketsList.getStack(i).getData().equals("*")) 
                counterMultiplication++;
        }
        if (counterMultiplication > 0) 
            multiplicationFound = true;
        return multiplicationFound;
    }

}
