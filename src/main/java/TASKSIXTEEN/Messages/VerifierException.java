package TASKSIXTEEN.Messages;

public class VerifierException extends Exception {

    public VerifierException(String message){
        super(message);
    }
    
    public VerifierException(String message, String operation){
        super(message+"\n\n\t\t\t -> Operation: "+operation+"\n\n");
    }
}
