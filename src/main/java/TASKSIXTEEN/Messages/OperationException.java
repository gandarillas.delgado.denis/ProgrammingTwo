package TASKSIXTEEN.Messages;

public class OperationException extends Exception {
    
    public OperationException(String messageException, int firtsNumber, int lastNumber, char operator){
        super(messageException+"\n\n\t\t\t -> OPERATION: "+firtsNumber+operator+lastNumber);
    }

    public OperationException(String messageException, int number, char operator){
        super(messageException+"\n\n\t\t\t -> OPERATION: "+number+operator);
    }

}
