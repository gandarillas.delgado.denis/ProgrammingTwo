package TASKINCLASS30AG;

public class Colita<T extends Comparable<T>> {
    
    private Node<T> raiz;
    private int size;
    private Node<T> head;

    public void addElement(T data){
        Node<T> aux = new Node<T>(data);
        if (size == 0) {
            raiz = aux;
            head = aux;
            size++;
        }else{
            aux.setNext(raiz);
            raiz = aux;
            size++;
        }
    }

    public static void main(String[] args) {
        Colita<Integer> cola = new Colita<>();
        cola.addElement(1);
        cola.addElement(2);
        cola.addElement(3);
        System.out.println(cola.toString());
    }

}
