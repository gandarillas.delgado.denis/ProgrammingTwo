package TASKTWELVE;

public class LinkedBag<T extends Comparable<T>> implements Bag<T> {
    private int size;
    private Node<T> root;

    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public void selectionSort() {
        for (int i = 0; i < size - 1; i++) {

            for (int j = i + 1; j < size; j++) {

                if (nodoGet(i).getData().compareTo(nodoGet(j).getData()) > 0) {

                    xchange(i, j);

                }

            }

        }

    }

    public void bubbleSort() {

        boolean verifyChanges = false;

        while (!verifyChanges) {

            verifyChanges = true;

            for (int i = 0; i < size - 1; i++) {

                if (nodoGet(i).getData().compareTo(nodoGet(i + 1).getData()) > 0) {

                    xchange(i + 1, i);
                    verifyChanges = false;

                }

            }

        }

    }

    @Override
    public void xchange(int y, int x) {

        Node<T> nodoX = root, nodoY = root, beforeX = null, beforeY = null, nodoAuxiliar;

        if (x != y) {

            for (int i = 0; i < x; i++) {

                if (nodoX != null) {

                    beforeX = nodoX;
                    nodoX = nodoX.getNext();

                }

            }
            for (int i = 0; i < y; i++) {

                if (nodoY != null) {

                    beforeY = nodoY;
                    nodoY = nodoY.getNext();

                }

            }

            if (nodoX != null || nodoY != null) {

                if (beforeX != null) {

                    beforeX.setNext(nodoY);

                } else {

                    root = nodoY;

                }
                if (beforeY != null) {

                    beforeY.setNext(nodoX);

                } else {

                    root = nodoX;

                }

                nodoAuxiliar = nodoX.getNext();
                nodoX.setNext(nodoY.getNext());
                nodoY.setNext(nodoAuxiliar);
            }

        }

    }

    // -------------------------------------------------
    // Own methods

    private Node<T> nodoGet(int index) {

        Node<T> nodoAux = root;
        Node<T> element = null;

        if (size > 0 && index < size) {
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    element = nodoAux;
                }
                nodoAux = nodoAux.getNext();
            }
        }

        return element;
    }

}