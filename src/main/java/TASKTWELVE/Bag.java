package TASKTWELVE;

public interface Bag<T extends Comparable<T>> {
    boolean add(T data);

    void selectionSort();

    void bubbleSort();

    void xchange(int y, int x);

}

class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> node) {
        next = node;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}