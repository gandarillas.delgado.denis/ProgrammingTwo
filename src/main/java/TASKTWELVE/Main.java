package TASKTWELVE;

public class Main {
    public static void main(String[] args) {
        Bag<Integer> bagSelection = new LinkedBag<>();
        bagSelection.add(5);
        bagSelection.add(4);
        bagSelection.add(1);
        bagSelection.add(2);
        bagSelection.add(3);
        bagSelection.add(7);
        bagSelection.add(9);
        bagSelection.add(6);
        bagSelection.add(8);
        System.out.println(bagSelection);
        bagSelection.selectionSort();
        System.out.println(bagSelection);

        System.out.println("------------------------------------------");

        Bag<Integer> bagBubble = new LinkedBag<>();
        bagBubble.add(5);
        bagBubble.add(4);
        bagBubble.add(9);
        bagBubble.add(6);
        bagBubble.add(8);
        bagBubble.add(1);
        bagBubble.add(2);
        bagBubble.add(3);
        bagBubble.add(7);
        System.out.println(bagBubble);
        bagBubble.bubbleSort();
        System.out.println(bagBubble);

    }
}
