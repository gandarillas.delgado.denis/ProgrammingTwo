package TASKTHREE;

public class Canvas {

    private int width, height;
    private String[][] canvaDraw;
    private int x1, x2, y1, y2;
    private static final String PLECA = "|";
    private static final String DASH = "-";
    private static final String EQUIS = "x";

    public Canvas(int width, int height) {

        this.width = width + 2;
        this.height = height + 2;
        this.canvaDraw = constructorCanva();
        this.x1 = 0;
        this.x2 = 0;
        this.y1 = 0;
        this.y2 = 0;

    }

    public String[][] constructorCanva() {
        canvaDraw = new String[getHeight()][getWidth()];

        drawBorder();
        draw(x1, y1, x2, y2);

        return canvaDraw;
    }

    public String[][] drawBorder() {

        for (int index = 0; index < height; index++) {

            if (index == 0 || index == height - 1) {
                for (int indexTwo = 0; indexTwo < width; indexTwo++) {

                    canvaDraw[index][indexTwo] = DASH;

                }
            }
            if (index > 0 && index < height - 1) {
                canvaDraw[index][0] = PLECA;
                canvaDraw[index][width - 1] = PLECA;
            }

        }
        return canvaDraw;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {

        this.x2 = x2;
        this.y2 = y2;
        this.x1 = x1;
        this.y1 = y1;

        if (x1 != x2 && y1 == y2) {
            x1 += 1;
            x2 += 2;
            y2 += 1;
            for (int i = x1; i < x2; i++) {
                canvaDraw[i][y2] = EQUIS;
            }
        }

        if (x1 == x2 && y1 != y2) {
            y1 += 1;
            y2 += 2;
            x2 += 1;
            for (int i = y1; i < y2; i++) {
                canvaDraw[x2][i] = EQUIS;
            }
        }

        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        return this;
    }

    public String drawCanvas() {

        String result = "";

        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                if (canvaDraw[i][j] == null) {
                    result = result + " ";
                }
                if (canvaDraw[i][j] != null) {
                    result = result + canvaDraw[i][j];
                }
            }
            if (i < height - 1) {
                result = result + "\n";
            }

        }

        return result;
    }

    public static void main(String[] args) {

        Canvas canvas = new Canvas(10, 10);
        canvas.draw(0, 5, 9, 5).draw(5, 0, 5, 9).draw(0, 2, 9, 2);
        System.out.println(canvas.drawCanvas());
    }

}