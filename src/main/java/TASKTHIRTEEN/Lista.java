package TASKTHIRTEEN;

public interface Lista<T extends Comparable<T>> {

    int size();

    boolean isEmpty();

    boolean add(T data);

    boolean remove(T data);

    T get(int index);

    void selectionSort();

    void bubbleSort();

    void mergeSort();

    void addElementsInteger();

    void rotate(int places);

}