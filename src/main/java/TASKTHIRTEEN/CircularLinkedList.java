package TASKTHIRTEEN;

public class CircularLinkedList<T extends Comparable<T>> implements Lista<T> {

    private Nodo<T> raiz;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return raiz == null;
    }

    @Override
    public T get(int index) {

        Nodo<T> nodoAux = raiz;

        for (int i = 0; i < index; i++) {

            nodoAux = nodoAux.getNext();

        }

        return nodoAux.getData();
    }

    @Override
    public void mergeSort() {

        int halfList = size() / 2;
        int[] left = new int[halfList];
        int[] right = new int[halfList];
        Nodo<T> nodoAuxiliar = raiz;

        for (int i = 0; i < size(); i++) {

            if (i >= 0 && i < halfList) {
                left[i] = (int) nodoAuxiliar.getData(); //It is forced to be an integer variable (int) because when entering a generic variable (T) it is not accepted by the array.
            }

            if (i >= halfList && i < size()) {
                right[i - (halfList)] = (int) nodoAuxiliar.getData(); //It is forced to be an integer variable (int) because when entering a generic variable (T) it is not accepted by the array.
            }

            nodoAuxiliar = nodoAuxiliar.getNext();

        }

        if (left.length % 2 == 0 && left.length > 2) {
            left = merge(left);
        } else {
            left = ordering(left);
        }

        if (right.length % 2 == 0 && right.length > 2) {
            right = merge(right);
        } else {
            right = ordering(right);
        }

        int[] arrayAux = new int[size];

        for (int i = 0; i < size; i++) {

            if (i >= 0 && i < halfList) {
                arrayAux[i] = left[i];
            }

            if (i >= halfList && i < size) {
                arrayAux[i] = right[i - halfList];
            }

        }

        while (!verifyOrdering(arrayAux)) {

            arrayAux = ordering(arrayAux);

        }

        int[] arrayFinal = arrayAux;

        addElements(arrayFinal);

    }

    // Second part------------------------------------

    @Override
    public void rotate(int places){

        boolean firstChangeCheck = false;
        Object auxiliar = 0;
        Object variableAuxiliar = 0;
        Nodo<T> nodoAuxiliar = raiz;
        int[] nodoArrays = new int[size];

        for (int i = 0; i < size; i++) {
            nodoArrays[i] = (int) nodoAuxiliar.getData();
            nodoAuxiliar = nodoAuxiliar.getNext();
        }

        if (places > 0) {

            for (int i = 0; i < places; i++) {
                for (int j = 0; j < nodoArrays.length - 1; j++) {

                    if (auxiliar.equals(0)) {
                        auxiliar = nodoArrays[j];
                    }

                    if (!firstChangeCheck) {
                        nodoArrays[0] = nodoArrays[nodoArrays.length - 1];
                        firstChangeCheck = true;
                    }

                    variableAuxiliar = nodoArrays[j + 1];

                    nodoArrays[j + 1] = (int) auxiliar;

                    auxiliar = variableAuxiliar;

                }
                firstChangeCheck = false;
            }

        }

        if (places < 0) {

            for (int i = places; i < 0; i++) {
                for (int j = nodoArrays.length; j > 0; j--) {

                    if (auxiliar.equals(0)) {
                        auxiliar = nodoArrays[j - 1];
                    }

                    if (!firstChangeCheck) {
                        nodoArrays[j - 1] = nodoArrays[0];
                        firstChangeCheck = true;
                    }

                    if (j > 1) {
                        variableAuxiliar = nodoArrays[j - 2];
                        nodoArrays[j - 2] = (int) auxiliar;

                        auxiliar = variableAuxiliar;
                    }

                }
                firstChangeCheck = false;
            }

        }

        addElements(nodoArrays);

    }

    // Help method

    @Override
    public void addElementsInteger() {
        raiz = new Nodo(10);
        Nodo<T> nodoTwo = new Nodo(1);
        Nodo<T> nodoThree = new Nodo(5);
        Nodo<T> nodoFour = new Nodo(8);
        Nodo<T> nodoFive = new Nodo(2);
        Nodo<T> nodoSix = new Nodo(13);
        Nodo<T> nodoSeven = new Nodo(1);
        Nodo<T> nodoEight = new Nodo(2);
        Nodo<T> nodoNine = new Nodo(11);
        Nodo<T> nodoTen = new Nodo(16);

        raiz.setNext(nodoTwo);
        nodoTwo.setNext(nodoThree);
        nodoThree.setNext(nodoFour);
        nodoFour.setNext(nodoFive);
        nodoFive.setNext(nodoSix);
        nodoSix.setNext(nodoSeven);
        nodoSeven.setNext(nodoEight);
        nodoEight.setNext(nodoNine);
        nodoNine.setNext(nodoTen);
        nodoTen.setNext(raiz);

        size = 10;

    }

    // Own methods

    private void addElements(int[] element) {

        raiz = new Nodo(element[0]);
        Nodo<T> nodoTwo = new Nodo(element[1]);
        Nodo<T> nodoThree = new Nodo(element[2]);
        Nodo<T> nodoFour = new Nodo(element[3]);
        Nodo<T> nodoFive = new Nodo(element[4]);
        Nodo<T> nodoSix = new Nodo(element[5]);
        Nodo<T> nodoSeven = new Nodo(element[6]);
        Nodo<T> nodoEight = new Nodo(element[7]);
        Nodo<T> nodoNine = new Nodo(element[8]);
        Nodo<T> nodoTen = new Nodo(element[9]);

        raiz.setNext(nodoTwo);
        nodoTwo.setNext(nodoThree);
        nodoThree.setNext(nodoFour);
        nodoFour.setNext(nodoFive);
        nodoFive.setNext(nodoSix);
        nodoSix.setNext(nodoSeven);
        nodoSeven.setNext(nodoEight);
        nodoEight.setNext(nodoNine);
        nodoNine.setNext(nodoTen);
        nodoTen.setNext(raiz);

    }

    public String toString() {
        return "Nodo [data=" + raiz.getData() + ", next=" + raiz.getNext() + "]";
    }

    public int[] merge(int[] arrayInteger) {

        int halfSize = arrayInteger.length / 2;

        int[] leftArray = new int[halfSize];
        int[] rightArray = new int[halfSize];

        for (int i = 0; i < arrayInteger.length; i++) {

            if (i >= 0 && i < halfSize) {
                leftArray[i] = arrayInteger[i];
            }

            if (i >= halfSize && i < arrayInteger.length) {
                rightArray[i - halfSize] = arrayInteger[i];
            }

        }

        while (!verifyOrdering(leftArray)) {
            if (leftArray.length % 2 != 0 || leftArray.length == 2) {
                leftArray = ordering(leftArray);
            } else {
                leftArray = merge(leftArray);
            }
        }

        while (!verifyOrdering(rightArray)) {
            if (rightArray.length % 2 != 0 || rightArray.length == 2) {
                rightArray = ordering(rightArray);
            } else {
                rightArray = merge(rightArray);
            }
        }

        int[] arrayAux = new int[arrayInteger.length];

        for (int i = 0; i < arrayInteger.length; i++) {

            if (i >= 0 && i < halfSize) {
                arrayAux[i] = leftArray[i];
            }

            if (i >= halfSize && i < arrayInteger.length) {
                arrayAux[i] = rightArray[i - halfSize];
            }

        }

        arrayInteger = arrayAux;

        return arrayInteger;
    }

    public int[] ordering(int[] list) {

        for (int i = 0; i < list.length; i++) {

            int firstNumber = list[i];

            for (int j = i + 1; j < list.length; j++) {

                int lastNumber = list[j];

                if (firstNumber > lastNumber) {

                    list[i] = lastNumber;
                    list[j] = firstNumber;

                    firstNumber = lastNumber;

                }

            }

        }

        return list;
    }

    public boolean verifyOrdering(int[] list) {

        int countNumbersOrder = 0;
        boolean verify = false;

        for (int i = 0; i < list.length - 1; i++) {

            if (list[i] < list[i + 1] || list[i] == list[i + 1]) {

                countNumbersOrder++;

            }

        }

        if (countNumbersOrder == list.length - 1) {

            verify = true;

        }

        return verify;

    }

    // Methods not required

    @Override
    public boolean add(T data) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean remove(T data) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void selectionSort() {
        // TODO Auto-generated method stub

    }

    @Override
    public void bubbleSort() {
        // TODO Auto-generated method stub

    }

}
