package TASKTHIRTEEN;

public class Main {
    
    public static void main(String[] args) {
        
        Lista<Integer> listaInteger = new CircularLinkedList<>();

        listaInteger.addElementsInteger();

        System.out.println("==========================================");
        System.out.println("==========================================");

        System.out.println("1");

        System.out.println("-------------------------------");

        for (int i = 0; i < listaInteger.size(); i++) {
            System.out.println(listaInteger.get(i));
        }

        System.out.println("==========================================");
        System.out.println("==========================================");

        System.out.println("2");

        System.out.println("-------------------------------");

        System.out.println(listaInteger.size());

        System.out.println("==========================================");
        System.out.println("==========================================");

        System.out.println("3");

        System.out.println("-------------------------------");

        listaInteger.rotate(5);

        for (int i = 0; i < listaInteger.size(); i++) {
            System.out.println(listaInteger.get(i));
        }

        System.out.println("==========================================");
        System.out.println("==========================================");

        System.out.println("4");

        System.out.println("-------------------------------");

        listaInteger.rotate(-7);
        
        for (int i = 0; i < listaInteger.size(); i++) {
            System.out.println(listaInteger.get(i));
        }

        listaInteger.mergeSort();

        System.out.println("==========================================");
        System.out.println("==========================================");

        System.out.println("5");

        System.out.println("-------------------------------");

        for (int i = 0; i < listaInteger.size(); i++) {
            System.out.println(listaInteger.get(i));
        }

        System.out.println("==========================================");
        System.out.println("==========================================");

    }

}