package TASK02SEP;

public class Queue<T extends Comparable<T>> {
    
    private T data;
    private Queue<T> next;
    
    public Queue(T data) {
        this.data = data;
    }
    
    public T getData() {
        return data;
    }
    
    public Queue<T> getNext() {
        return next;
    }
    
    public void setNext(Queue<T> next) {
        this.next = next;
    }
    
    @Override
    public String toString() {
        return "Queue [data=" + data + ", next=" + next + "]";
    }
    
}

//Implementar una estructura tipo Cola que no permita elementos duplicados.