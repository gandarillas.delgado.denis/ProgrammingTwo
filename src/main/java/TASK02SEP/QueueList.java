package TASK02SEP;

public class QueueList<T extends Comparable<T>> {

    private Queue<T> raiz;
    private int size;
    private Queue<T> head;

    public void enqueue(T data) {
        Queue<T> aux = new Queue<T>(data);
        Queue<T> auxiliar = raiz;
        if (size == 0) {
            raiz = aux;
            head = aux;
            size++;
        } else {
            for (int i = 0; i < size; i++) {

                if (data == auxiliar.getData()) {

                    return;

                }

                auxiliar = auxiliar.getNext();

            }

            aux.setNext(raiz);
            raiz = aux;
            size++;

        }
    }

    public void enqueue2(T data) {
        Queue<T> aux = new Queue<T>(data);
        Queue<T> auxiliar = raiz;
        if (size == 0) {
            raiz = aux;
            head = aux;
            size++;
        } else {
            for (int i = 0; i < size; i++) {

                if (auxiliar.getData().compareTo(data) == 0) {

                    return;

                }

                auxiliar = auxiliar.getNext();

            }

            aux.setNext(raiz);
            raiz = aux;
            size++;

        }
    }

    @Override
    public String toString() {
        return "Colita [head=" + head + "]\n[raiz=" + raiz + "]\n[size=" + size + "]";
    }

    public static void main(String[] args) {

        QueueList<Integer> queueList = new QueueList<>();

        queueList.enqueue(1);
        queueList.enqueue(2);
        queueList.enqueue(5);
        queueList.enqueue(3);
        queueList.enqueue(1);
        queueList.enqueue(5);

        System.out.println(queueList.toString());

        System.out.println("MEJORA------------- ");
        QueueList<Integer> queueList3 = new QueueList<>();

        queueList3.enqueue2(1);
        queueList3.enqueue2(2);
        queueList3.enqueue2(1);
        queueList3.enqueue2(3);
        queueList3.enqueue2(1);
        queueList3.enqueue2(4);

        System.out.println(queueList3.toString());

        QueueList<String> queueList2 = new QueueList<>();

        queueList2.enqueue2("DENIS");
        queueList2.enqueue2("Pedro");
        queueList2.enqueue2("DENIS");
        queueList2.enqueue2("JOTA");
        queueList2.enqueue2("Denis");

        System.out.println("STRINGS----------");
        System.out.println(queueList2.toString());

    }

}
