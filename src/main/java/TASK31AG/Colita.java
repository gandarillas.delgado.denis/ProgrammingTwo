package TASK31AG;

public class Colita<T extends Comparable<T>> {
    
    private Node<T> raiz;
    private int size;
    private Node<T> head;

    public void addElement(T data){
        Node<T> aux = new Node<T>(data);
        if (size == 0) {
            raiz = aux;
            head = aux;
            size++;
        }else{
            aux.setNext(raiz);
            raiz = aux;
            size++;
        }
    }

    public void remove(){
        Node<T> auxiliar = getNode(size - 1);
        if (size > 1) {
            auxiliar.setNext(null);
            head = auxiliar;
            size--;
            Node<T> auxRaiz = getNode(size - 1);
            auxRaiz.setNext(auxiliar);
        }else if (size == 1) {
            raiz.setNext(null);
            head = auxiliar;
            size--;
        }else{
            raiz = null;
            head = null;
        }
    }

    private Node<T> getNode(int index) {
        Node<T> nodoAux = raiz;
        Node<T> element = null;

        if (!(size == 0) && index < size) {
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    element = nodoAux;
                }
                nodoAux = nodoAux.getNext();
            }
        }

        return element;
    }

    

    @Override
    public String toString() {
        return "Colita [head=" + head + "]\n[raiz=" + raiz + "]\n[size=" + size + "]";
    }

    public static void main(String[] args) {
        Colita<Integer> cola = new Colita<>();
        System.out.println("ADD-----------------");
        cola.addElement(1);
        cola.addElement(2);
        cola.addElement(3);
        cola.addElement(4);
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
        System.out.println("ADD-----------------");
        cola.addElement(5);
        System.out.println(cola.toString());
        System.out.println("REMOVE-----------------");
        cola.remove();
        System.out.println(cola.toString());
    }

}