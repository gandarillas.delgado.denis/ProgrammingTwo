package TASKFIFTEEN;

public class Stack<T extends Comparable<T>> implements ListaNode<T> {

    private Node<T> raiz;
    private int size;

    @Override
    public T push(T element) {

        Node<T> nodeAux = new Node<T>(element);

        if (size == 0) {
            raiz = nodeAux;
            size++;
        } else {
            Node<T> lastNode = getNode(size - 1);

            lastNode.setNext(nodeAux);

            size++;
        }

        return element;

    }

    @Override
    public T pop() {

        T elementRemove = null;

        if (size == 1) {

            elementRemove = raiz.getData();
            raiz = null;
            size = 0;

        }

        if (size >= 2) {

            Node<T> nodeAux = getNode(size - 2);

            elementRemove = nodeAux.getNext().getData();
            nodeAux.setNext(null);

            size--;

        }

        return elementRemove;
    }

    // Own methods

    private Node<T> getNode(int index) {
        Node<T> nodoAux = raiz;
        Node<T> element = null;

        if (!(size == 0) && index < size) {
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    element = nodoAux;
                }
                nodoAux = nodoAux.getNext();
            }
        }

        return element;
    }

}
