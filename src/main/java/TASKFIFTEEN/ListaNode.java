package TASKFIFTEEN;

public interface ListaNode<T extends Comparable<T>> {

    T push(T element);

    T pop();

}
