package TASKFIFTEEN;

public class Main {

    public static void main(String[] args) {
        ListaNode<Integer> listaNode = new Stack<>();

        listaNode.push(2);
        listaNode.push(23);
        listaNode.push(14);
        listaNode.push(13);

        System.out.println(listaNode.pop());
        System.out.println(listaNode.pop());
        System.out.println(listaNode.pop());
        System.out.println(listaNode.pop());
        System.out.println(listaNode.pop());
    }

}
